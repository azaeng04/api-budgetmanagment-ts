process.env.PORT = 3001;
process.env.HOST_NAME = 'localhost';
process.env.DB_USERNAME = 'sa';
process.env.SA_PASSWORD= '@za0412Michal';
process.env.DATABASE_NAME = 'BudgetManagementDev';

module.exports = function (wallaby) {
    return {
      files: [
        'src/**/*.json',
        'src/**/*.yaml',
        'src/**/*.js',
        'src/**/*.ts',
        'src/*.js',
        'src/*.json',
        '!./wallaby.js',
        '.sequelizerc',
        './__tests__/**/*.ts',
        '!./__tests__/**/**/*.test.ts'
      ],
  
      tests: [
        './__tests__/**/**/*.test.ts'
      ],
      env: {
        type: 'node'
      },
      testFramework: 'jest',
      instrumented: false,
      workers: {
        recycle: true
      },
      compilers: {
        '**/*.ts?(x)': wallaby.compilers.typeScript({
          module: 'commonjs',
          useStandardDefaults: true,
          typescript: require('typescript'),
          isolatedModules: true,
        })
      },
    };
  };