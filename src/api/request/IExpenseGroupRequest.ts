'use strict';

export interface IExpenseGroupCreateRequest {
    name: string;
    description: string;
}

export interface IExpenseGroupUpdateRequest {
    id: number;
    name?: string;
    description?: string;
}
