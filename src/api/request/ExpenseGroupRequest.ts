import { IExpenseGroupCreateRequest } from '.';

export class ExpenseGroupCreateRequest implements IExpenseGroupCreateRequest {
    name: string;
    description: string;
}
