'use strict';

import { IExpenseGroupResponse } from '../../response';

export class ExpenseGroupResponse implements IExpenseGroupResponse {
  public id: number;
  public name: string;
  public description: string;

  constructor(builder: ExpenseGroupResponseBuilder) {
    this.id = builder.id;
    this.name = builder.name;
    this.description = builder.description;
  }
}

export class ExpenseGroupResponseBuilder implements IExpenseGroupResponse {
  public id: number;
  public name: string;
  public description: string;

  constructor(id: number, name: string, description: string) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  public build() {
    return new ExpenseGroupResponse(this);
  }
}
