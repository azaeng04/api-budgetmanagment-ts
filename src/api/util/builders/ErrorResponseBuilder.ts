'use strict';

import { IErrorResponse, ErrorResponse } from '../../response';

export class ErrorResponseBuilder extends Error implements IErrorResponse {
    public statusCode: number;
    public message: string;

    constructor(statusCode: number, message?: string) {
        super(message);
        this.statusCode = statusCode;
    }

    public build() {
        return new ErrorResponse(this);
    }
}
