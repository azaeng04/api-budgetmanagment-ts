'use strict';

import { Route, Controller, Get, Query, SuccessResponse, Tags, Response, Post, Body, Put, Delete , Request} from 'tsoa';
import { ExpenseGroup } from '../../models';
import { ExpenseGroupResponseBuilder, ErrorResponseBuilder } from '../util/builders';
import { IErrorResponse, IExpenseGroupResponse } from '../response';
import { IExpenseGroupCreateRequest } from '../request';
import { IExpenseGroupUpdateRequest } from '../request/IExpenseGroupRequest';
import { Op } from 'sequelize';
import * as express from 'express';
import { ExpenseGroupResponse } from '../response/ExpenseGroupResponse';

@Route('expenseGroup')
export class ExpenseGroupController extends Controller {
    @SuccessResponse(201)
    @Post()
    @Tags('Expense Group')
    public async createExpenseGroup(@Request() request: express.Request): Promise<void> {
        try {
            await ExpenseGroup.create(request.body);
            this.setStatus(201);
        } catch (e) {
            this.setStatus(500);
            throw new ErrorResponseBuilder(500, `The following error occurred: ${e.message}`);
        }
    }

    @Response<IErrorResponse>(404)
    @SuccessResponse(200)
    @Get('{id}')
    @Tags('Expense Group')
    public async getExpenseGroupById(id: number, @Request() request: express.Request)
    : Promise<IExpenseGroupResponse | IErrorResponse> {
        try {
            const expenseGroup = await ExpenseGroup.findByPk(id);
            if(expenseGroup === null || expenseGroup === undefined || id <= 0) {
                this.setStatus(404);
                return new ErrorResponseBuilder(404, `Expense Group with id ${id} not found`).build();
            }
            const { name, description } = expenseGroup;

            this.setStatus(200);
            return new ExpenseGroupResponseBuilder(expenseGroup.id, name, description).build();
        } catch (e) {
            this.setStatus(500);
            return new ErrorResponseBuilder(500, `The following error occurred: ${e.message}`).build();
        }
    }

    @Put()
    @SuccessResponse(204)
    @Tags('Expense Group')
    public async updateExpenseGroup(@Body() body: IExpenseGroupUpdateRequest): Promise<void> {
        const query = { where: { id: { [Op.eq]: body.id } } };
        const newExpenseGroup = { ...body };

        await ExpenseGroup.update(newExpenseGroup, query);

        this.setStatus(204);
    }

    @Delete('{id}')
    @SuccessResponse(204)
    @Tags('Expense Group')
    public async deleteExpenseGroupById(id: number): Promise<void> {
        const query = { where: { id: { [Op.eq]: id } } };

        await ExpenseGroup.destroy(query);

        this.setStatus(204);
    }
}

@Route('expenseGroups')
export class ExpenseGroupsController extends Controller {
    @SuccessResponse(200)
    @Get()
    @Tags('Expense Group')
    public async getExpenseGroups(): Promise<IExpenseGroupResponse[]> {
        const query = { attributes: { exclude: ['createdAt', 'updatedAt', 'deletedAt'] } };
        return ExpenseGroup.findAll(query);
    }
}
