import { IErrorResponse } from '../response';
import { ErrorResponseBuilder } from '../util/builders';

export class ErrorResponse implements IErrorResponse {
    public statusCode: number;
    public message: string;

    constructor(builder: ErrorResponseBuilder) {
        this.statusCode = builder.statusCode;
        this.message = builder.message;
    }
}
