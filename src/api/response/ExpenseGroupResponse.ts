import { IExpenseGroupResponse } from './interface/IExpenseGroupResponse';

export class ExpenseGroupResponse implements IExpenseGroupResponse {
    id: number;
    name: string;
    description: string;
}
