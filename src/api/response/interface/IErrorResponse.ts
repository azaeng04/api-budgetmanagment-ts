'use strict';

import { IMessageResponse } from '..';

export interface IErrorResponse extends IMessageResponse {
}
