'use strict';

export interface IExpenseGroupResponse {
  id: number;
  name: string;
  description: string;
}
