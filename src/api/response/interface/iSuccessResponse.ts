'use strict';

import { IMessageResponse } from '..';

export interface ISuccessResponse extends IMessageResponse {
}
