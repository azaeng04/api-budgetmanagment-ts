'use strict';

export interface IMessageResponse {
    statusCode: number;
    message?: string;
}
