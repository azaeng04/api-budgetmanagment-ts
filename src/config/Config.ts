import { SequelizeOptions } from 'sequelize-typescript';
import { Dialect } from 'sequelize';

export class Config implements SequelizeOptions {
    server: string = process.env.HOST_NAME;
    database: string = process.env.DATABASE_NAME;
    username: string = process.env.DB_USERNAME;
    password: string = process.env.SA_PASSWORD;
    dialect: Dialect = 'mssql';
    modelPaths: string [] = [ __dirname + '../models/*.model'];
}
