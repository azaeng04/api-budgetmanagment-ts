'use strict';

process.env.PORT = '3001';
process.env.HOST_NAME = 'localhost';
process.env.DB_USERNAME = 'sa';
process.env.SA_PASSWORD = '@za0412Michal';
process.env.DATABASE_NAME = 'BudgetManagementDev';

import { ServerStartUp } from './server-setup';
import { Server } from './server';
import { createModels } from './create-models';
import '../tsoa.ts';

createModels();

const server = new Server();

export default new ServerStartUp(server.core, process.env.PORT);
