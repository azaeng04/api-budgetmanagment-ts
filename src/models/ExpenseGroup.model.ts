'use strict';

import {
  Table,
  Model,
  Column,
  Sequelize,
  CreatedAt,
  UpdatedAt,
  DeletedAt,
  AutoIncrement,
  HasMany,
  DataType} from 'sequelize-typescript';
import { Expense } from './Expense.model';

@Table
export class ExpenseGroup extends Model<ExpenseGroup> {
  @AutoIncrement
  @Column({ primaryKey: true })
  public id: number;

  @Column({ type: DataType.STRING, allowNull: false })
  public name: string;

  @Column({ type: DataType.STRING, allowNull: false })
  public description: string;

  @CreatedAt
  public createdAt: Date;

  @UpdatedAt
  public updatedAt: Date;

  @DeletedAt
  public deletedAt: Date;

  @HasMany(() => Expense, { as: 'expenses', constraints: false })
  public expenses: Expense[];
}
