import { Sequelize } from 'sequelize-typescript';
import { Config } from '../config/Config';
import { ExpenseGroup } from './ExpenseGroup.model';
import { Expense } from './Expense.model';

const sequelize = new Sequelize(new Config());
sequelize.addModels([ ExpenseGroup, Expense ]);

export {
    sequelize,
    ExpenseGroup,
    Expense,
};
