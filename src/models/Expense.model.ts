import { Model, Column, DataType, Table, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { ExpenseGroup } from '.';

@Table
export class Expense extends Model<Expense> {
    @Column({ type: DataType.STRING })
    public name: string;

    @Column({ type: DataType.STRING })
    public description: string;

    @BelongsTo(() => ExpenseGroup, { constraints: false })
    public expenseGroup: ExpenseGroup;

    @ForeignKey(() => ExpenseGroup)
    @Column({ type: DataType.INTEGER })
    public expenseGroupId: number;
}
