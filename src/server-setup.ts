import * as errorHandler from 'api-error-handler';
import debug from 'debug';
import * as express from 'express';
import * as http from 'http';
import { AddressInfo } from 'net';

debug('ts-express:server');

export class ServerStartUp {
    public app: express.Application;
    public port: number | string | boolean;
    public server: http.Server;

    constructor(app: express.Application, port: number | string) {
        this.port = this.retrievePort(port);
        this.app = app;
        this.server = this.createServer();
        this.server.on('error', this.onError);
        this.server.on('listening', this.onListening);
        this.app.use(errorHandler());
        this.app.listen(this.port, () => {
            this.app.get('/', (req, res) =>
                res.send(`Node and express server is running on port ${this.port}`)
            );
            console.log(`Listening on ${this.port}`);
        });
    }

    public retrievePort(port: number | string): number | string | boolean {
        return this.normalizePort(process.env.PORT || port);
    }

    public normalizePort(val: number | string): number | string | boolean {
        const port: number = typeof val === 'string' ? parseInt(val, 10) : val;
        if (port === undefined || port === null) {
            return false;
        } else if (isNaN(port)) {
            return val;
        } else if (port >= 0) {
            return port;
        }
    }

    public onError(error: NodeJS.ErrnoException, port: number | string): void {
        if (error.syscall !== 'listen') {
            throw error;
        }
        const bind: string = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;
        switch (error.code) {
            case 'EACCES':
                debug(`${bind} requires elevated privileges`);
                process.exit(1);
                break;
            case 'EADDRINUSE':
                debug(`${bind} is already in use`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    public createServer() {
        return http.createServer();
    }

    public onListening(server: http.Server): void {
        const addr: string | AddressInfo = server.address();
        const bind: string = typeof addr === 'string' ? `Pipe ${addr}` : `Port ${addr.port}`;
        console.log(`Listening on ${bind}`);
    }
}
