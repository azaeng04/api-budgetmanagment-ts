import * as express from 'express';
import { serve, setup } from 'swagger-ui-express';
import { RegisterRoutes } from './routes';
const swaggerDoc = require('./api/swagger/swagger.json');

import './api/controllers/ExpenseGroup.controller';

export class Server {
  public core = express();

  constructor() {
    this.registerSwagger(this.core);
  }

  public registerSwagger(app: any): void {
    app.use('/api-docs', serve, setup(swaggerDoc));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    RegisterRoutes(this.core);
  }
}

export default new Server();
