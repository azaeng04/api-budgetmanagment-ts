import { sequelize } from './models';

function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function createModels() {
    delay(10000).then(() => {
        sequelize.sync({
            force: true
        }).then(() => {
            console.log('Syncronized database models successfully');
        }).catch((err) => {
            console.log(`Failed to syncronize database due to the following error ${err.message}`);
        });
    }).catch((onReject: Error) => {
        console.log(`The following error occurred ${onReject.message}`);
    });
}
