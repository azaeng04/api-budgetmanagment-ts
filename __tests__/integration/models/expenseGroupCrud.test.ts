import { merge } from 'lodash';
import { ExpenseGroup, sequelize } from '../../../src/models';
import { expenseGroup } from './data/expenseGroup.payload';
import { Op } from 'sequelize';

describe('Expense Group CRUD', () => {
    beforeAll(async () => {
        await sequelize.sync({ force: true });
    });
    test('can create an Expense Group', async () => {
        const result = await ExpenseGroup.create(expenseGroup);

        expect(result).toHaveProperty('id');
        expect(result).toMatchObject(expenseGroup);
    });

    test('can read an Expense Group', async () => {
        const result = await ExpenseGroup.create(expenseGroup);

        const retrievedExpenseGroup = await ExpenseGroup.findByPk(result.id);

        expect(retrievedExpenseGroup).toMatchObject(expenseGroup);
    });

    test('can update an Expense Group', async () => {
        const result = await ExpenseGroup.create(expenseGroup);
        const updatedValues = { name: 'Toiletries', description: 'This is Toiletries' };
        const query = { where: { id: { [Op.eq]: result.id } } };

        const updatedExpGroup = merge(expenseGroup, updatedValues);

        await ExpenseGroup.update(updatedExpGroup, query);
        const updatedExpenseGroup = await ExpenseGroup.findByPk(result.id, { raw: true });

        expect(updatedExpenseGroup).toMatchObject(updatedValues);
    });

    test('can delete an Expense Group', async () => {
        const result = await ExpenseGroup.create(expenseGroup);
        const query = { where: { id: { [Op.eq]: result.id } } };

        await ExpenseGroup.destroy(query);

        const noExpenseGroup = await ExpenseGroup.findByPk(result.id, { raw: true });

        expect(noExpenseGroup).toBeNull();
    });
});
