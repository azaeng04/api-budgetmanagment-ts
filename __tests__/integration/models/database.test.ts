import { sequelize } from '../../../src/models';

describe('Database Operations', () => {
    test('can connect to the Database', async () => {
        const expectedMessage = 'Connection has been established successfully';

        const message = await sequelize.authenticate()
          .then(() => {
            return 'Connection has been established successfully';
          }).catch((err) => {
            return `Unable to connect to the database. The following error occurred: ${err}`;
          });
        
        expect(message).toEqual(expectedMessage);
    });
});
