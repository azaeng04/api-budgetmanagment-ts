'use strict';
import { ServerStartUp } from '../../src/server-setup';
import server from '../../src/server';

describe('Server Test', () => {
    test('can start up server', async () => {
        process.env.PORT = '3002';
        new ServerStartUp(server.core, 0);
    });
});
