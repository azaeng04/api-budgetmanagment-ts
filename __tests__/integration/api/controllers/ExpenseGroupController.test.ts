import * as rp from 'request-promise-native';

describe('', () => {
    test('', async () => {
        const body = { name: 'Test', description: 'Test' };
        const response = await rp.post({
            uri: 'http://localhost:3001/api/expenseGroup',
            body: JSON.stringify(body),
            resolveWithFullResponse: true,
            headers: {
                'Content-Type': 'application/json'
            }
        }).catch((err) => err);

        expect(response.statusCode).toEqual(201);
    });
});
