import { generateRoutes, generateSwaggerSpec, RoutesConfig, SwaggerConfig } from 'tsoa';

(async () => {
  const swaggerOptions: SwaggerConfig = {
    name: 'api-budget-management',
    basePath: '/api/',
    entryFile: './src/index.ts',
    specVersion: 3,
    outputDirectory: './src/api/swagger',
    controllerPathGlobs: ['./src/api/controllers/*.ts'],
    yaml: false,
    host: `${process.env.HOST_NAME}:${process.env.PORT}`,
    schemes: ['http'],
  };

  const routeOptions: RoutesConfig = {
    basePath: '/api/',
    entryFile: './src/index.ts',
    routesDir: './src/',
  };

  await generateSwaggerSpec(swaggerOptions, routeOptions);

  await generateRoutes(routeOptions, swaggerOptions);
  console.log('Swagger document generated successfully');
})();
